const textMath = require('markdown-it-texmath');

const sidebar = {
  generalPhysics: {
    title: 'Общая физика',
    collapsable: false,
    children: [
      '/general-physics/fundamentals',
      '/general-physics/kinematics',
      '/general-physics/dynamic',
      '/general-physics/pressure',
    ],
  },
};

module.exports = {
  title: 'Конспекты по физике',
  head: [
    [
      'link',
      {
        rel: 'stylesheet',
        href: 'https://cdn.jsdelivr.net/npm/katex/dist/katex.min.css',
      },
    ]
  ],
  themeConfig: {
    sidebar: ['/', sidebar.generalPhysics],
  },
  dest: 'public',
  base: process.env.NODE_ENV === 'production' ? '/physics-notes/' : '/',
  markdown: {
    extendMarkdown: (md) => {
      md.use(require('markdown-it-imsize'));
      md.use(textMath, {
        engine: require('katex'),
        delimiters: 'gitlab',
        katexOptions: { macros: { '\\RR': '\\mathbb{R}' } },
      });
    },
  },
};
